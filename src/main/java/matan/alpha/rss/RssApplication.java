package matan.alpha.rss;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import matan.alpha.rss.feeds.processors.IFeedDataProcessor;
import matan.alpha.rss.threads.FetcherCommand;

@SpringBootApplication
public class RssApplication  {

	static Logger log = Logger.getLogger(RssApplication.class.getName());
	
	public static void main(String[] args) {
		
		log.info("Start!");
		log.info("arguments: "+args);
		if(args.length>0)
		{
			
			String feedUrl = args[0];
			ApplicationContext context =
			    	  new ClassPathXmlApplicationContext(new String[] {"ConfigBean.xml"});
			
			IFeedDataProcessor feedDataProcessor = (IFeedDataProcessor) context.getBean("FeedDataProcessorProfile");
			feedDataProcessor.setFeedUrl(feedUrl);
			FetcherCommand.setFeedDataProcessor(feedDataProcessor);
			SpringApplication.run(RssApplication.class, args);

		}
		else
		{
			System.out.println("Usage: Rss FeedUrl");
			log.error("No arguments was found going to exit program!");
		}
		
	}


}

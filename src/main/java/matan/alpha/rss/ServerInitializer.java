package matan.alpha.rss;

import org.apache.log4j.Logger;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import matan.alpha.rss.threads.FetcherRunner;

@Component
public class ServerInitializer implements ApplicationRunner {

	static Logger log = Logger.getLogger(ServerInitializer.class.getName());
	
	/*
	 * Start the feed fetcher thread only after system is up. 
	 */
	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("Spring boot is up. Going to start fetcher thread.");
		new Thread(new FetcherRunner()).start();
	}

}

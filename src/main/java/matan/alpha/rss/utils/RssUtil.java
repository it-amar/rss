package matan.alpha.rss.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

public class RssUtil {

	private static String timeStampFormat="yyyy-MM-dd HH:mm:ss.SSS";
	
	static Logger log = Logger.getLogger(RssUtil.class.getName());
	final static  String timeStampFileExtention = ".txt"; 
	
	private static void saveToFile( String fileName, String timeStampText,String feedUrl) {
		List<String> lines = readFromFile(fileName);
		
		String newLine = feedUrl+timeStampText;
		boolean lineWasFound=false;
		for (int j = 0; j < lines.size(); j++) {
			String line = lines.get(j);
			if(line.startsWith(feedUrl))
			{
				lines.set(j, newLine);
				lineWasFound=true;
				break;
			}
		}
		if(!lineWasFound)
		{
			lines.add(newLine);
		}
		
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName+timeStampFileExtention))) {
			for (String line:lines) {
				writer.write(line+"\n");
			}
        }catch (Exception e) {
        	log.error("When trying to save file "+fileName+" got "+e.getMessage());
		} 
	}
	
	public static Date getTimeStamp(String feedName,String feedUrl)
	{
		Date ret=null;
		final List<String> lines = readFromFile(feedName);
		String savedTime =null;
		for (String line:lines) {
			if(line.startsWith(feedUrl))
			{
				savedTime =line.replace(feedUrl, "");
			}
		}
		
		if(savedTime!=null)
		{
			SimpleDateFormat spdf = new SimpleDateFormat(timeStampFormat);
			try {
				ret = spdf.parse(savedTime);
			} catch (ParseException e) {
				log.error("When trying to parse "+savedTime+" got "+e.getMessage());
			}
		}
		return ret;
	}
	
	public static void saveTimeStamp(String feedName,Date timeStamp,String feedUrl)
	{
		SimpleDateFormat spdf = new SimpleDateFormat(timeStampFormat);
		String timeStampText=spdf.format(timeStamp);
		saveToFile(feedName, timeStampText,feedUrl);
	}
	

	
	private static List<String> readFromFile( String fileName) {
		List<String>ret= new ArrayList();
		try (BufferedReader writer = Files.newBufferedReader(Paths.get(fileName+timeStampFileExtention))) {
			String line = writer.readLine();
			ret.add(line);
        }catch (Exception e) {
        	log.error("When trying to read from file "+fileName+" got "+e.getMessage());
		} 
		return ret;
	}
}

package matan.alpha.rss.fetchers;

import matan.alpha.rss.feeds.data.GenericFeedData;

public interface IFeedFecherWrapper {

	GenericFeedData fetch(String url);

}
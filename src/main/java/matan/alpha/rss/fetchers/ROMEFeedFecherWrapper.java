package matan.alpha.rss.fetchers;

import java.net.URL;
import java.util.List;

import org.apache.log4j.Logger;

import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;

import matan.alpha.rss.feeds.data.GenericFeedData;
import matan.alpha.rss.feeds.data.GenericFeedEntryData;

public class ROMEFeedFecherWrapper extends BaseFeedFecherWrapper {

	static Logger log = Logger.getLogger(ROMEFeedFecherWrapper.class.getName());
	@Override
	public GenericFeedData fetch(String url) {
		
		GenericFeedData genericFeed = null;
		
		
		try {
			log.info("Going to retrive feed for url: "+url);
			URL feedUrl = new URL(url);
			
			SyndFeedInput input = new SyndFeedInput();
			SyndFeed feed = input.build(new XmlReader(feedUrl));
			genericFeed = new GenericFeedData(feed.getTitle(),feed.getDescription());
			List entries = feed.getEntries();
			log.info("Number of entries found for feed with url: "+url+" is: "+feed.getEntries().size());
			
			for (Object entry:entries) {
				SyndEntry syndicatEntry =  (SyndEntry)entry;
				SyndContent description = syndicatEntry.getDescription();
				String descriptionValue ="";
				if(description!=null)
				{
					descriptionValue = description.getValue();	
				}
				
				GenericFeedEntryData feedEntry= new GenericFeedEntryData(syndicatEntry.getTitle(),descriptionValue,syndicatEntry.getLink(),syndicatEntry.getPublishedDate());
				genericFeed.addEntry(feedEntry);
			}
			
		} catch (Exception e) {
			log.error("When tring to read feed got "+e.getMessage());
		}
		
		return genericFeed;
	}

}

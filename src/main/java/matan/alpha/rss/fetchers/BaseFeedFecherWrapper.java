package matan.alpha.rss.fetchers;

import matan.alpha.rss.feeds.data.GenericFeedData;

public abstract class BaseFeedFecherWrapper implements IFeedFecherWrapper {

	@Override
	public abstract GenericFeedData fetch(String url);
	
	
}

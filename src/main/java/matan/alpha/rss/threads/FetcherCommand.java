package matan.alpha.rss.threads;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import matan.alpha.rss.feeds.data.GenericFeedData;
import matan.alpha.rss.feeds.processors.FeedProceesReturn;
import matan.alpha.rss.feeds.processors.IFeedDataProcessor;
import matan.alpha.rss.fetchers.IFeedFecherWrapper;
import matan.alpha.rss.fetchers.ROMEFeedFecherWrapper;
import matan.alpha.rss.queue.QueueManager;
import matan.alpha.rss.utils.RssUtil;

/*
 * To separate the call to fetch feed from the running thread. 
 */
public class FetcherCommand {

	static Logger log = Logger.getLogger(FetcherRunner.class.getName());
	
	
	private static IFeedDataProcessor feedDataProcessor =null;

	public static void setFeedDataProcessor(IFeedDataProcessor feedDataProcessor) {
		FetcherCommand.feedDataProcessor = feedDataProcessor;
	}

	
	private IFeedFecherWrapper feedFetcher=null;
	
	
	
	public void setFeedFetcher(IFeedFecherWrapper feedFetcher) {
		this.feedFetcher = feedFetcher;
	}


	public void excute()
	{
		log.info("Start fetch command");
		Date timeStamp = new Date(0);
		String feedUrl = feedDataProcessor.getFeedUrl();
		String feedName = feedDataProcessor.getFeedName();
		log.info("Going to read timestamp for feed with name "+feedName+" Feed url "+feedUrl);
		Date savedTimeStamp = RssUtil.getTimeStamp(feedName,feedUrl);
		if(savedTimeStamp!=null)
		{
			timeStamp=savedTimeStamp;
		}
		log.info("Time stamp is: "+timeStamp);
		
		/*
		 * For test only let the Dummy feed be ready. 
		 */
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			log.error("When sleeping got "+e.getMessage());
		}
		
		while (true) {
			
			GenericFeedData fetch = feedFetcher.fetch(feedUrl);
			FeedProceesReturn processed = feedDataProcessor.process(fetch, timeStamp);
			
			if(processed!=null)
			{
				timeStamp=processed.getTimestamp();
				timeStamp = adjustTimeStamp(timeStamp);
				fetch=processed.getFeedData();
				
				if(fetch.getFeedsCount()>0 || QueueManager.feddQueue.isEmpty())
				{
					log.info("Queue feed with "+fetch.getFeedsCount()+" entries");
					QueueManager.feddQueue.add(fetch);	
				}else 
				{
					log.info("Queue is not empty and no entries to display");
				}
				RssUtil.saveTimeStamp(feedName, timeStamp,feedUrl);
				log.info("Feed for url "+feedUrl +" Fetched");
			}else
			{
				log.error("Rss was not found");
			}
			try {
				Thread.sleep(feedDataProcessor.getWaitForNextFetch());
			} catch (InterruptedException e) {
				log.error("When sleeping got "+e.getMessage());
			}
		}

	}


	/*
	 * Move timestamp of last feed found a second forward 
	 * so it will not be included in the next request.
	 */
	private Date adjustTimeStamp(Date timeStamp) {
		Calendar instance = Calendar.getInstance();
		instance.setTime(timeStamp);
		instance.add(Calendar.SECOND,1);
		timeStamp=instance.getTime();
		return timeStamp;
	}
}

package matan.alpha.rss.threads;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class FetcherRunner implements Runnable {

	static Logger log = Logger.getLogger(FetcherRunner.class.getName());

	
	
	
	@Override
	public void run() {
		ApplicationContext context =
		    	  new ClassPathXmlApplicationContext(new String[] {"ConfigBean.xml"});
		
		
		String commandProfileName = "FetcherCommandProfile";
		log.info("Going to read fetcher command with profile "+commandProfileName);
		FetcherCommand fetcherCommand = (FetcherCommand) context.getBean(commandProfileName);
		log.info("Start command fetcher for profile "+commandProfileName);
		fetcherCommand.excute();

	}

}

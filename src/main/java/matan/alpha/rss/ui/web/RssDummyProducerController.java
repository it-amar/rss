package matan.alpha.rss.ui.web;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sun.syndication.feed.synd.SyndContent;
import com.sun.syndication.feed.synd.SyndContentImpl;
import com.sun.syndication.feed.synd.SyndEntry;
import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.feed.synd.SyndFeedImpl;
import com.sun.syndication.io.SyndFeedOutput;


/*
 * For test only generate dummy feeds
 * URL should be http://localhost:8080/DummyFeed
 * 
 */

@RestController
public class RssDummyProducerController {

    
	static Logger log = Logger.getLogger(RssDummyProducerController.class.getName());
	
	int numberOfentriesToCreate = 10;
	
    @RequestMapping("/DummyFeed")
    public String Rss() {
    	
    	log.info("DummyFeed was called number of entries to create "+numberOfentriesToCreate);
    	
    	SyndFeed feed = new SyndFeedImpl();
    	feed.setFeedType("rss_1.0");

    	feed.setTitle("Sample Feed (created with ROME)");
    	feed.setLink("http://rome.dev.java.net");
    	feed.setDescription("This feed has been created using ROME (Java syndication utilities");
    	
    	List entries = new ArrayList();
    	SyndEntry entry;
    	SyndContent description;

    	
		for (int i = 0; i < numberOfentriesToCreate; i++) {
        	entry = new SyndEntryImpl();
        	entry.setTitle("Item "+i);
        	entry.setLink("https://www.google.co.il/webhp?q=Go"+i);
        	entry.setPublishedDate(new Date());
        	description = new SyndContentImpl();
        	description.setType("text/plain");
        	description.setValue("Item "+i+" description");
        	entry.setDescription(description);
        	entries.add(entry);
			
		}
    	feed.setEntries(entries);
    	
    	StringWriter writer = new StringWriter();
        SyndFeedOutput output = new SyndFeedOutput();
        try {
			output.output(feed,writer);
			writer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	
    	
        return writer.getBuffer().toString();
    }

}
package matan.alpha.rss.ui.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import matan.alpha.rss.feeds.data.GenericFeedData;
import matan.alpha.rss.feeds.data.GenericFeedEntryData;
import matan.alpha.rss.queue.QueueManager;


@Controller
public class RssDisplayController {

 
	static Logger log = Logger.getLogger(RssDisplayController.class.getName());
    
    @RequestMapping("/ShowFeed")
    public String greeting(@RequestParam(value="name", required=false, defaultValue="World") String name, Model model) {
    	
    	log.info("ShowFeed controler was called");
    	String feedTitle="Currently no title";
    	String feedDescription="Currently no description";
    	boolean isTitleSet=false;
    	List<GenericFeedEntryData> entryList = new ArrayList<>();
    	while (!QueueManager.feddQueue.isEmpty()) {
			GenericFeedData genericFeed = (GenericFeedData) QueueManager.feddQueue.poll();
			if(!isTitleSet)
			{
		    	feedTitle=genericFeed.getTitle();
		    	feedDescription=genericFeed.getDescription();
			}
			Iterator<GenericFeedEntryData> feeds = genericFeed.getFeeds();
			while (feeds.hasNext()) {
				GenericFeedEntryData genericFeedEntry = (GenericFeedEntryData) feeds.next();
				entryList.add(genericFeedEntry);
			}
			
		}
    	
    	log.info("Number of entries to display "+entryList.size());
        model.addAttribute("feedTitle", feedTitle);
        model.addAttribute("feedDescription", feedDescription);
        model.addAttribute("entryList", entryList);
        return "showFeed";
    }
    

}
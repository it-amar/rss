package matan.alpha.rss.feeds.processors;

import java.util.Date;

import matan.alpha.rss.feeds.data.GenericFeedData;

public class FeedProceesReturn
{
	
	
	
	public FeedProceesReturn(GenericFeedData feedData, Date timestamp) {
		super();
		this.feedData = feedData;
		this.timestamp = timestamp;
	}
	private GenericFeedData feedData;
	private Date timestamp;
	public GenericFeedData getFeedData() {
		return feedData;
	}
	public void setFeedData(GenericFeedData feedData) {
		this.feedData = feedData;
	}
	public Date getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public String toString() {
		return "FeedProceesReturn [feedData=" + feedData + ", timestamp=" + timestamp + "]";
	}
	
	
	
}
package matan.alpha.rss.feeds.processors;

import java.util.Date;

import matan.alpha.rss.feeds.data.GenericFeedData;

/*
 * Make sure only unread feeds will be saved.
 * Clean feeds entries with issues like missing title and more..
 * Leave option to override cleaning option based on a specific feed. 
 */
public interface IFeedDataProcessor {

	long getWaitForNextFetch();

	String getFeedUrl();

	String getFeedName();

	void setFeedUrl(String feedUrl);

	FeedProceesReturn process(GenericFeedData feedData, Date timeStamp);

}
package matan.alpha.rss.feeds.processors;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import matan.alpha.rss.feeds.data.GenericFeedData;
import matan.alpha.rss.feeds.data.GenericFeedEntryData;

public class BaseFeedDataProcessor implements IFeedDataProcessor {

		static Logger log = Logger.getLogger(BaseFeedDataProcessor.class.getName());
	
		private String feedUrl=null;
		private long waitForNextFetch=300000;
		private String feedName=null;
		
		
		
		
		@Override
		public long getWaitForNextFetch() {
			return waitForNextFetch;
		}




		public void setWaitForNextFetch(long waitForNextFetch) {
			this.waitForNextFetch = waitForNextFetch;
		}


		@Override
		public String getFeedUrl() {
			return feedUrl;
		}
		
		
		
		@Override
		public String getFeedName() {
			return feedName;
		}




		public void setFeedName(String feedName) {
			this.feedName = feedName;
		}


		@Override
		public void setFeedUrl(String feedUrl) {
			this.feedUrl = feedUrl;
		}


		
		
		protected GenericFeedData checkAndSetFeedData(GenericFeedData feedData)
		{
			GenericFeedData ret=null;
			if(feedData!=null)
			{
				if(feedData.getTitle()!=null && feedData.getDescription()!=null)
				{
					ret= new GenericFeedData(feedData.getTitle(), feedData.getDescription());
				}
			}
			return ret;
		}
		
		protected GenericFeedEntryData checkAndSetFeedEntryData(GenericFeedEntryData feedEntryData)
		{
			GenericFeedEntryData ret=null;
			if(feedEntryData.getTitle()!=null && feedEntryData.getDescription()!=null && feedEntryData.getLink()!=null && feedEntryData.getPublishedDate()!=null)
			{
				ret = new GenericFeedEntryData(feedEntryData);
	
			}
			return ret;
		}
		
		@Override
		public FeedProceesReturn process(GenericFeedData feedData,Date timeStamp)
		{
			log.info("Start feed process");
			log.info("Number of read entries "+feedData.getFeedsCount());
			GenericFeedData retFeedData=checkAndSetFeedData(feedData);
			List<GenericFeedEntryData> list = new ArrayList<>();
			if(retFeedData!=null)
			{
				Iterator<GenericFeedEntryData> feeds = feedData.getFeeds();
				while (feeds.hasNext()) {
					GenericFeedEntryData genericFeedEntryData = (GenericFeedEntryData) feeds.next();
					GenericFeedEntryData feedEntryData = checkAndSetFeedEntryData(genericFeedEntryData);
					if(feedEntryData!=null)
					{
						list.add(feedEntryData);
					}
				}
			}
			FeedProceesReturn ret=null;
			if(retFeedData!=null)
			{
				Collections.sort(list);
				for (GenericFeedEntryData feedEntryData:list) {
					if(feedEntryData.getPublishedDate().compareTo(timeStamp)>=0)
					{
						retFeedData.addEntry(feedEntryData);	
						timeStamp=feedEntryData.getPublishedDate();
					}
				}
				ret= new FeedProceesReturn(retFeedData, timeStamp);
				log.info("Number of entries to display "+retFeedData.getFeedsCount());
			}
			
			return ret;
			
			
		}
}

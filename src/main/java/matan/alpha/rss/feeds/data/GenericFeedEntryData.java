package matan.alpha.rss.feeds.data;

import java.util.Date;

public class GenericFeedEntryData implements Comparable<GenericFeedEntryData>  {
	
	private String title;
	private String description;
	private String link;
	private Date publishedDate;
	
	
	
	
	
	public GenericFeedEntryData(GenericFeedEntryData genericFeedEntryData) {
		super();
		this.title = genericFeedEntryData.title;
		this.description = genericFeedEntryData.description;
		this.link = genericFeedEntryData.link;
		this.publishedDate = genericFeedEntryData.publishedDate;
	}
	public GenericFeedEntryData(String title, String description, String link, Date publishedDate) {
		super();
		this.title = title;
		this.description = description;
		this.link = link;
		this.publishedDate = publishedDate;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getPublishedDate() {
		return publishedDate;
	}
	public void setPublishedDate(Date publishedDate) {
		this.publishedDate = publishedDate;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public String toString() {
		return "GenericFeedEntry [title=" + title + ", description=" + description + ", link=" + link
				+ ", publishedDate=" + publishedDate + "]";
	}

	@Override
	public int compareTo(GenericFeedEntryData o) {
		return this.getPublishedDate().compareTo(o.getPublishedDate());
	}

	
}

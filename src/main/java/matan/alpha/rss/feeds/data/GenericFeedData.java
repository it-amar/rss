package matan.alpha.rss.feeds.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class GenericFeedData {

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	private String title;
	private String description;

	


	public GenericFeedData(String title, String description) {
		super();
		this.title = title;
		this.description = description;
	}

	private List<GenericFeedEntryData>feeds = new ArrayList<>();
	
	public GenericFeedData() {
		super();
	}

	public Iterator<GenericFeedEntryData> getFeeds() {
		return feeds.iterator();
	}
	
	public int getFeedsCount() {
		return feeds.size();
	}

	public void addEntry(GenericFeedEntryData genericFeed) {
		feeds.add(genericFeed);
	}

	
	@Override
	public String toString() {
		return "GenericFeed [title=" + title + ", description=" + description + ", feeds=" + feeds + "]";
	}
	
}
